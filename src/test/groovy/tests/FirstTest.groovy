package tests

import groovy.util.logging.Slf4j
import org.junit.Test
/*
 * Created with IntelliJ IDEA.
 * User: mfo
 * Date: 10/26/14
 * Time: 4:12 PM
 *
 * To change this template use File | Settings | File Templates.
 */

@Slf4j
class FirstTest {

    @Test
    void testOne() {
        Thread.sleep(100)
    }

    @Test
    void testTwo() {
        Thread.sleep(100)
        assert false
    }

    @Test
    void testThree() {
        Thread.sleep(100)
    }

    @Test
    void testFour() {
        Thread.sleep(100)
    }

    @Test
    void testFive() {
        Thread.sleep(100)
    }

    @Test
    void testSix() {
        Thread.sleep(100)
    }

}
